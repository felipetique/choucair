package co.com.utest.proyectobase.stepdefinitions;



import co.com.utest.model.UtestData;
import co.com.utest.questions.Answer;
import co.com.utest.tasks.Login;
import co.com.utest.tasks.OpenUp;
import co.com.utest.tasks.Search;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;


public class UtestStepDefinitions {

    public static void setStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^Since Felipe wants to learn automation on the uTest page$")
    public void sinceFelipeWantsToLearnAutomationOnTheUTestPage(List<UtestData> utestData)throws Exception {
        OnStage.theActorCalled("brandon").wasAbleTo(OpenUp.thepage(),
                Login.onThePage(utestData.get(0).getStrUser(),utestData.get(0).getStrPassword()));
    }

    @When("^you search the uTest page and register on the Utest platform$")
    public void youSearchTheUTestPageAndRegisterOnTheUtestPlatform(List<UtestData> utestData)throws Exception {
            OnStage.theActorInTheSpotlight().attemptsTo(Search.the(utestData.get(0).getStrTest()));
    }

    @Then("^perform the tests on the uTest page$")
    public void performTheTestsOnTheUTestPage(List<UtestData> utestData)throws Exception{
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(utestData.get(0).getStrTest())));
    }

}
