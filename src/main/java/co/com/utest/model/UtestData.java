package co.com.utest.model;

public class UtestData {
    private String strUser;
    private String strPassword;
    private String strTest;


    public String getStrUser() {
        return strUser;
    }
    public void setStrUser(String strUser) {
        this.strUser = strUser;
    }

    public String getStrPassword() {
        return strPassword;
    }

    public void setStrPassword(String strPassword) {
        this.strPassword = strPassword;
    }

    public String getStrTest() {
        return strTest;
    }

    public void setStrTest(String strTest) {
        this.strTest = strTest;
    }



}
