package co.com.utest.tasks;

import co.com.utest.userinterface.SearchUtestPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Search implements Task {

    private String test;


    public Search(String test) {
        this.test = test;
    }
    public static Search the(String test) {
        return Tasks.instrumented(Search.class);

    }


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(SearchUtestPage.BUTTON_UC),
                Enter.theValue(test).into(SearchUtestPage.INPUT_COURSE),
                Click.on(SearchUtestPage.BUTTON_GO),
                Click.on(SearchUtestPage.SELECT_COURSE)

        );

    }
}
