package co.com.utest.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SearchUtestPage  {

    public static final Target BUTTON_UC = Target.the("seleccione ")
            .located(By.xpath("//div[@id='test']//strong"));

    public static final Target INPUT_COURSE = Target.the("Buscar ")
            .located(By.id("testsearchbox"));

    public static final Target BUTTON_GO = Target.the("Da click para buscar el test")
            .located(By.id("//button[@class='btn btn-secondary']"));

    public static final Target SELECT_COURSE = Target.the("Da click para buscar el test")
            .located(By.xpath("//h4[contains(text(),'Test')]"));

    public static final Target NAME_COURSE = Target.the("Extraer el nombre del test").located(By.xpath("//h1[contans(text(),'Utest')]"));

}
